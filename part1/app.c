/*
 * lab 4.c
 *
 * Created: 6/7/2011 12:57:25 PM
 *  Author: dcstanc
 */ 

#define F_CPU	16000000
#include <util/delay.h>
#include<avr/interrupt.h>
#include <avr/io.h>
#include "kernel.h"

#define NULL (void *) 0

// Base frequency for the buzzer
#define BASE_FREQ	200

//my variables
unsigned loval,hival,adcval;

unsigned int chan1_adc=0;
unsigned int chan0_adc=0;
unsigned int channel;
unsigned int buzzer_val=0;
unsigned int led_val=500;

//useful functions

int remap(int val, int min_val, int max_val)
{
	val= (val-min_val)/(max_val-min_val)*255;
	
	if(val<0)
	{return 0;}
		else if(val>255){return 255;}
			else{return val;}
}


//end useful functions

void task1(void *p)
{
	// Operates the buzzer
	// The base frequency is passed in through p.
	int base=(int) p;
	
	while(1)
	{

		// Implement Task 1 logic below
			int i;
			//init code
			int freq=(buzzer_val/255*300)+base;
			int delay=500/freq;
			int loops=500/delay;
			//end init code
	
			for(i=0;i<loops;i++)
			{
				//write to digi pin 13, portb pin 5
				PORTB|=0b00100000;
		
				_delay_ms(delay);
		
				PORTB&=0b11011111;
		
				_delay_ms(delay);
			}
		// Implement Task 1 logic above
		
		// Hand over control to the OS.
		OSSwapTask();
	}		
}

void task2(void *p)
{
	while(1)
	{
		// Implement Task 2 logic below
			//led_val=(chan1_adc*750/1023)+250;
			led_val=chan1_adc;
		// Implement Task 2 logic above
		
		// Hand over control to the OS.
		OSSwapTask();
	}		
}

void task3(void *p)
{
	while(1)
	{
		// Implement Task 3 logic below
			buzzer_val=remap(chan0_adc,620,890);
		// Implement Task 3 logic above
		
		// Hand over control to the OS.
		OSSwapTask();
	}		
}

void task4(void *ptr)
{
	while(1)
	{
		// Implement Task 4 logic below
						
			PORTB&=0b11110111; //TURN OFF LED
			_delay_ms(led_val);
			PORTB|=~(0b11110111); //TURN ON LED
			_delay_ms(led_val);
		// Implement Task 4 logic above
		
		// Hand over control to the OS.
		OSSwapTask();
	}	
	
}

// Declare all other ISRs, etc below this line.

ISR(ADC_vect)
{
	loval=ADCL;
	hival=ADCH;
	adcval=hival*256+loval;
	if(channel==1){
	
	chan1_adc=adcval;
	channel=0;
	}
	else{
	
	chan0_adc=adcval;
	channel=1;
	 }	 
	startADC(channel);
}
// Declare all other ISRs, etc above this line.

//modularize setup and start
void setupADC(){
	PRR&=0b11111110;
	
	//prescaler of 64, enable interrupts
	ADCSRA=0b10001110;
	
	ADMUX=0b01000001;//set reference voltage and defaulT of channel 1
}

void startADC(int chan)
{
	
	channel=chan;
	if(chan==0)
	ADMUX=0b01000000; //set channel in ADMUX
	else
	ADMUX=0b01000001;
	
	ADCSRA|=0b01000000; //start adc by setting bit 6 (ADSC) to 1
	
}

void setup()
{
	// Set up the hardware except those used directly by the OS (e.g. Timer 0).
	DDRB|=0b00101000;
	
	setupADC();
	
	startADC(1);
	sei();
}

// The stack sizes may need adjusting!!
int task1Stack[100], task2Stack[100], task3Stack[100],task4Stack[100];

int main()
{
	setup();
	
	OSInit();

	// Create the tasks. Task1 is for the buzzer and we pass in the base frequency as an argument.
	OSAddTask(task1, 0, &task1Stack[99], (void *) BASE_FREQ);
	OSAddTask(task2, 1, &task2Stack[99], NULL);
	OSAddTask(task3, 2, &task3Stack[99], NULL);
	
	// Insert code to create task4 below.
	OSAddTask(task4, 3, &task4Stack[99], NULL);
	// And start the OS!
	OSRun();
}
